const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.post('/login', async (req, res) => {
  const user = await User.findOne({
    where: { username: req.body.username, password: req.body.password },
  });

  if (user) {
    const token = jwt.sign({ userId: user.id }, 'secreto', { expiresIn: '1h' });
    res.json({ token });
  } else {
    res.status(401).json({ message: 'Invalid credentials' });
  }
});

module.exports = router;