const express = require('express');
const router = express.Router();
const PruebaB = require('../models/pruebaB');
const auth = require('../middleware/auth');

// Recibir todos los registros
router.get('/', auth, async (req, res) => {
    try {
      const myTableData = await PruebaB.findAll();
      res.json(myTableData);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  });
  
// Crear un nuevo registro
router.post('/', auth, async (req, res) => {
    try {
      const newRecord = await PruebaB.create(req.body);
      res.json(newRecord);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
});
  
  // Actualizar un registro
router.put('/:id', auth, async (req, res) => {
    try {
      const updatedRecord = await PruebaB.update(req.body, {
        where: { id: req.params.id },
      });
      res.json(updatedRecord);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
});
  
  // Eliminar un registro
router.delete('/:id', auth, async (req, res) => {
    try {
      await PruebaB.destroy({
        where: { id: req.params.id },
      });
      res.json({ message: 'Registro eliminado exitosamente' });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
});

module.exports = router;