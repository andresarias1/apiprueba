const express = require('express');
const authRouter = require('./routes/auth');
const myTableRouter = require('./routes/pruebaB');

const app = express();

// Middleware 
app.use(express.json());
app.use('/auth', authRouter);
app.use('/pruebaB', authRouter, myTableRouter); // Protect routes with authentication

// Error 
app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

const port = process.env.PORT || 5000;
app.listen(port, () =>
 
console.log(`Server listening on port ${port}`));