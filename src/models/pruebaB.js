const Sequelize = require('sequelize');
const config = require('../config');


const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: 'tedious',
});

const PruebaB = sequelize.define('prueba', {
  code: {
    type: Sequelize.INTEGER,
  },
  name: {
    type: Sequelize.STRING,
  },
  date: {
    type: Sequelize.DATE,
  },
});

module.exports = PruebaB;