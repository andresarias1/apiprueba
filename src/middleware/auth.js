const jwt = require('express-jwt');

const auth = jwt({
  secret: 'secreto',  // Replace with a strong, unique secret key
  getToken: (req) => req.headers.authorization?.split(' ')[1],
});

module.exports = auth;